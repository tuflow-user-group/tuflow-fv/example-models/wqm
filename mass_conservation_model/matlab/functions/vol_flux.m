function [vf] = vol_flux(fl,NL,zl,area)

[dzs] = cell_dz(NL,zl);
flux = sum(fl,3);

vf(1:size(flux,2)) = 0;
for j = 1:size(flux,2)
    for i = 1:size(NL,1)
        start = sum(NL(1:i-1)) + 1;
        for k = start:(start + NL(i)) - 1
            vf(j) = vf(j) + sum(area(i)*(flux(k,j).*dzs(k,j)));
        end
    end
end


end
