from . import load_inputs, WQMass, WQOutput


def run_mass_balance_mgl(X_cc, X_ncon, X_pcon, X_scon, amm_nit_atm_split,
                         k_fres, K_fdom, f_pr, X_nrpom, X_prpom, f_pho, out_dir, id):
    suffix = '_WQ.nc'
    startdate = [2011, 5, 1, 0, 0, 0]
    tol = 0.1  # Plotting tolerance
    save_all = 1
    fig_count = 1
    if 'MGL' in id:
        glb = load_inputs.loadglobal(startdate, tol, save_all, X_cc, X_ncon, X_pcon, X_scon, amm_nit_atm_split,
                                     k_fres, K_fdom, f_pr, X_nrpom, X_prpom, f_pho, fig_count)
    else:
        print('Keyword: MGL not found in run_ID, exiting')
        return
    input = load_inputs.read_netcdf(out_dir, id, suffix)
    area, NL, zfaces = load_inputs.read_geometry(input)
    load_inputs.load_parameters(input, id, glb)
    if glb.sim_class < 0:
        print('unrecognized simulation class')
        return
    nc_names, phy_names, num_phy, phy_model, \
    pth_names_alive, pth_names_dead, pth_names_attch, \
    num_pth, pth_model  = load_inputs.construct_array(input, glb.sim_class)
    WQVar = load_inputs.load_variables(input, num_phy, phy_names,
                                       num_pth, pth_names_alive, pth_names_dead, pth_names_attch, glb)
    WQDiag = load_inputs.load_diagnostics(nc_names, input, num_phy, num_pth, pth_model, glb, WQVar, NL, zfaces)
    flux, mass = WQMass.compute_fluxes(input, WQDiag, WQVar, num_pth, glb, NL, area, zfaces)
    massbal = WQMass.do_mass_balance(mass, flux, glb.tol, glb.sim_class)
    WQOutput.write_xlsx(flux, massbal, input, out_dir, id, glb.sim_class)
    WQOutput.plot_mass_balance(input, massbal, glb.sim_class, id)


def run_mass_balance_mmm(X_cc, X_ncon, X_pcon, X_scon, amm_nit_atm_split,
                         k_fres, K_fdom, f_pr, X_nrpom, X_prpom, f_pho, out_dir, id):
    suffix = '_WQ.nc'
    startdate = [2011, 5, 1, 0, 0, 0]
    tol = 0.1  # Plotting tolerance
    save_all = 1
    fig_count = 1
    if 'MGL' in id:
        print('Keyword: MMM not found in run_ID, exiting')
        return
    else:
        X_ncon = [i * j * 14 / 12 for i, j in zip(X_ncon, X_cc)]
        X_pcon = [i * j * 31 / 12 for i, j in zip(X_pcon, X_cc)]
        X_scon = [i * j * 28 / 12 for i, j in zip(X_scon, X_cc)]
    glb = load_inputs.loadglobal(startdate, tol, save_all, X_cc, X_ncon, X_pcon, X_scon, amm_nit_atm_split,
                                     k_fres, K_fdom, f_pr, X_nrpom, X_prpom, f_pho, fig_count)
    input = load_inputs.read_netcdf(out_dir, id, suffix)
    area, NL, zfaces = load_inputs.read_geometry(input)
    load_inputs.load_parameters(input, id, glb)
    if glb.sim_class < 0:
        print('unrecognized simulation class')
        return
    nc_names, phy_names, num_phy, phy_model, \
    pth_names_alive, pth_names_dead, pth_names_attch, \
    num_pth, pth_model  = load_inputs.construct_array_mmm(input, glb.sim_class)
    WQVar = load_inputs.load_variables_mmm(input, num_phy, phy_names,
                                       num_pth, pth_names_alive, pth_names_dead, pth_names_attch, glb)
    WQDiag = load_inputs.load_diagnostics_mmm(nc_names, input, num_phy, num_pth, pth_model, glb, WQVar, NL, zfaces)
    flux, mass = WQMass.compute_fluxes_mmm(input, WQDiag, WQVar, num_pth, glb, NL, area, zfaces)
    massbal = WQMass.do_mass_balance(mass, flux, glb.tol, glb.sim_class)
    WQOutput.write_xlsx(flux, massbal, input, out_dir, id, glb.sim_class)
    WQOutput.plot_mass_balance(input, massbal, glb.sim_class, id)
