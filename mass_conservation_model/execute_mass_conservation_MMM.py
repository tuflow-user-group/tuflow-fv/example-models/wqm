## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# User inputs
# Do not change the array sizes
# These values must match the values specified in the WQ Module control files
# Not all values below are always used
# TUFLOW 2023

from python import run_mass
import os

# Uncomment one runID
run_ID = 'MB_DO_MMM'
#run_ID = 'MB_inorganics_MMM'
#run_ID = 'MB_organics_MMM'

# Alter these parameters if changes are made to the git repository fvwqm
# control file parameters file/s
X_cc_phy = [26.8, 27.8]                     # Phytoplankton carbon to chlorophyll a ratio [X_{cc}^{phy}]                                                            https://wqm-manual.tuflow.com/AppParams.html  # WQParXcc
X_N_C_con_phy = [0.0, 0.09249744]           # Phytoplankton constant nitrogen to chlorophyll a ratio [X_{N-C-con}^{phy}]. Set to 0.0 for advanced phyto group.      https://wqm-manual.tuflow.com/AppParams.html#WQParConNCRat
X_P_C_con_phy = [0.0, 0.00835461]           # Phytoplankton constant phosphorus to chlorophyll a ratio [X_{P-C-con}^{phy}]. Set to 0.0 for advanced phyto group.    https://wqm-manual.tuflow.com/AppParams.html#WQParConPCRat
X_Si_C_con_phy = [0.12793177, 0.12332991]   # Phytoplankton constant silicate to chlorophyll a ratio [X_{Si-C-con}^{phy}]. Set to 0.0 if unused.                    https://wqm-manual.tuflow.com/AppParams.html#WQParConSiCRat
f_TN_NO3 = 0.5                              # Fraction of atmospheric nitrogen deposition that is nitrate [f_{TN}^{NO_3}].                                          https://wqm-manual.tuflow.com/AppParams.html#WQParAtmWetNFrac
f_true_resp_phy = [0.2, 0.3]                # Fraction of phytoplankton respiration that is true respiration [f_{true-resp}^{phy}].                                 https://wqm-manual.tuflow.com/AppParams.html#WQParftrueresp
f_excr_loss_phy = [0.3, 0.4]                # Fraction of phytoplankton loss that is excretion [f_{excr-loss}^{phy}].                                               https://wqm-manual.tuflow.com/AppParams.html#WQParfdomloss
f_exud_phy = [0.1, 0.5]                     # Fraction of primary production lost to exudation [f_{exud}^{phy}].                                                    https://wqm-manual.tuflow.com/AppParams.html#WQParfracexud
X_N_RPOM = 0.1                              # Ratio of nitrogen to carbon in refractory organic matter [X_{N}^{RPOM}].                                              https://wqm-manual.tuflow.com/AppParams.html#WQParXNRPOM
X_P_RPOM = 0.01                             # Ratio of phosphorus to carbon in refractory organic matter [X_{P}^{RPOM}].                                            https://wqm-manual.tuflow.com/AppParams.html#WQParXPRPOM
f_photo_RDOM = 0.5                          # RDOM photolysis fraction [f_{photo}^{RDOM}].                                                                          https://wqm-manual.tuflow.com/AppParams.html#WQParfphotoDOM


# Do not change anything below here
out_dir = os.getcwd() + r'\results'
run_mass.run_mass_balance_mmm(X_cc_phy, X_N_C_con_phy, X_P_C_con_phy, X_Si_C_con_phy, f_TN_NO3,
                              f_true_resp_phy, f_excr_loss_phy, f_exud_phy, X_N_RPOM, X_P_RPOM,
                              f_photo_RDOM, out_dir, run_ID)
